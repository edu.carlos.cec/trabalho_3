const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const db = require('./src/configs/sequelize')
const exphbs = require('express-handlebars')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))


app.use(express.static('public'))

/*
app.engine('handlebars', exphbs())
app.set('view engine', 'handlebars')
*/


db.sequelize.authenticate().then(() => {
    console.log("Banco iniciado")
}).catch(err => {
    console.log(err + " Não foi possivel conectar!!!")
})

require('./src/clientes/routes')(app)
require('./src/historico/routes')(app)
require('./src/pedidos/routes')(app)
require('./src/produtos/routes')(app)

app.get("/", (req, res) => {
    res.sendFile(__dirname + "/public/views/login.html")
})

/*
app.get("/index", (req, res) => {
    //Leitura do cookie  --- redirecionar para login
    res.render("template" , {nome: "Carlos Eduardo Cabral"})
})
*/

app.get("/cadastro", (req, res) => {
    res.sendFile(__dirname + "/public/views/cadastro.html")
})


app.get("/atualizar", (req, res) => {
    res.sendFile(__dirname + "/public/views/atualizar.html")
})



var server = app.listen(3000, () => {
    console.log("Servidor rodando na porta " + server.address.port + " no host " + server.address().address)
})
