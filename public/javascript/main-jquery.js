
Usuario = {
    criaUsuario: () => {
        let t = {}
        t.nome = $("#nome").val()
        t.cpf = $("#cpf").val()
        t.telefone = $("#telefone").val()
        t.endereco = $("#endereco").val()
        t.dt_nascimento = $("#dataNascimento").val()
        t.usuario = $("#nome").val()
        t.senha  = $("#senha").val()
        t.email = $("#email").val()
        
        
        $.ajax({
            type: 'POST',
            url:  '/clientes',
            data: t,
            dataType: 'json',
            success: (cliente) => {
                alert(cliente)
            }
        })
    },
    
    login: () => {
        let t = {}
        t.usuario = $("#usuario").val()
        console.log(t.usuario)
        t.senha   = $("#senha").val()
        console.log(t.senha)

        $.ajax({
            type: 'GET',
            url:  '/clientes',
            data: t,
            dataType: 'json',
            success: (data) => {
                console.log(data)
                //Senha do objeto cliente igual a do informado pelo usuário
                if(data.msgCode == 1){
                    alert("Deu certo")
                    //Gravar um cookie
                    Cookies.set('usuario' , {id : data.id , nome : data.nome})
                    console.log(Cookies.get('usuario'))
                    location.replace("/index")
                }else{
                    alert("Deu Errado")
                }

            }
        })
    },

    preencherCampos: () => {
        let id = {}
        let u = JSON.parse(Cookies.get('usuario'))
        //console.log(u["id"])
        id.idArgumento = u["id"]

        $.ajax({
            type: 'GET',
            url:  '/clientes/id',
            data: id,
            dataType: 'json',
            success: (data) => {
                efetivarCampos(data)
            }
        })
    },

    atualizar: () => {
        let t = {}
        t.id = JSON.parse(Cookies.get("usuario"))["id"]
        
        t.nome = $("#nome").val()
        t.cpf = $("#cpf").val()
        t.telefone = $("#telefone").val()
        t.endereco = $("#endereco").val()
        t.dt_nascimento = $("#dataNascimento").val()
        t.usuario = $("#nome").val()
        t.senha  = $("#senha").val()
        t.email = $("#email").val()

        $.ajax({
            type: 'PUT',
            url:  '/clientes',
            data: t,
            dataType: 'json',
            success: (cliente) => {
                alert("DEU CERTO")
            }
        })
    }
}


function efetivarCampos(data){
    $("#usuario").val(data["nome"])
    $("#senha").val(data["senha"])
    $("#email").val(data["email"])
    $("#nome").val(data["usuario"])
    $("#cpf").val(data["cpf"])
    $("#dataNascimento").val(data["data_nascimento"])
    $("#endereco").val(data["endereco"])
    $("#telefone").val(data["telefone"])
}