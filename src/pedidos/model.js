const db = require("./../configs/sequelize")
const { Model, DataTypes } = db.Sequelize

const sequelize = db.sequelize

class Pedidos extends Model { }
Pedidos.init({
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true
    },
    item: {
        type: DataTypes.INTEGER
    },
    id_produto: {
        type: DataTypes.INTEGER
    },
    qtde: {
        type: DataTypes.INTEGER
    }

}, { sequelize, modelName: "pedidos" })

module.exports = Pedidos