const db = require("./../configs/sequelize")
const Clientes = require("./model")

const { Op } = db.Sequelize

exports.create = (req, res) => {
    Clientes.findOrCreate({
        where: {
            [Op.or]: [{ cpf: { [Op.like]: req.body.cpf } },

            { usuario: { [Op.like]: req.body.usuario } },
            { email:   { [Op.like]: req.body.email }},

            { usuario: { [Op.like]: req.body.usuario } }]

        },
        defaults: {
            nome: req.body.nome,
            cpf: req.body.cpf,
            telefone: req.body.telefone,
            endereco: req.body.endereco,
            dt_nascimento: req.body.dt_nascimento,
            usuario: req.body.usuario,
            senha: req.body.senha,
            email: req.body.email
        }
    }).then((clientes) => {
        res.send(clientes)
    })
}


//Usar no login
exports.buscarId = (req, res) => {
    console.log("usuario.....: " + req.query.usuario)
    Clientes.findOne({
        where: {
            usuario: req.query.usuario,
            senha: req.query.senha
        }
    }).then((cliente) => {
        if(cliente){
            res.send({msgCode: 1, id: cliente.id, nome: cliente.nome})
        }else{
            res.send({msgCode: 2})
        }
    }).catch(err=>{
        console.log("Buscar...: "+err)
    })
}


//Buscar por id
exports.buscarPorId = (req, res) => {
    Clientes.findOne({
        where: {
            id: req.query.idArgumento
        }
    }).then((cliente) => {
        res.send(cliente)
    })
}


exports.buscarId = (req, res) => {
    Clientes.findOne({
        where: {
            usuario: req.body.usuario
        }
    }).then((cliente) => {
        res.send(cliente)
    })
}


exports.remover = (req, res) => {
    Clientes.destroy({
        where: {
            id: req.body.id
        }
    }).then((affectedRows) => {
        res.send({ 'message': 'ok', 'affectedRows': affectedRows })
    })
}

exports.update = (req, res) => {
    Clientes.update({
        nome: req.body.nome,
        cpf: req.body.cpf,
        telefone: req.body.telefone,
        endereco: req.body.endereco,
        dt_nascimento: req.body.dt_nascimento,
        usuario: req.body.usuario,
        senha: req.body.senha,
        email: req.body.email
    },
        {
            where: {
                id: req.body.id
            }
        }).then((clientes) => {
            res.send(clientes)
        })
}