const db = require("./../configs/sequelize")
const { Model, DataTypes } = db.Sequelize

const sequelize = db.sequelize

class Clientes extends Model { }
Clientes.init({
    nome: {
        type: DataTypes.STRING,

    },
    cpf: {
        type: DataTypes.STRING
    },

    telefone: {
        type: DataTypes.STRING
    },
    endereco: {
        type: DataTypes.STRING
    },
    dt_nascimento: {
        type: DataTypes.DATE
    },
    usuario: {
        type: DataTypes.STRING
    },
    senha: {
        type: DataTypes.STRING
    },
    email: {
        type: DataTypes.STRING
    }

}, { sequelize, modelName: "clientes" })

module.exports = Clientes
